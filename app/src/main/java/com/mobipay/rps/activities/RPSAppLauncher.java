package com.mobipay.rps.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import com.mobipay.rps.R;
import com.pesachoice.billpay.activities.PCAppLauncher;



/**
 * Created by emmy on 11/21/17.
 */

public class RPSAppLauncher extends PCAppLauncher {

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.rps_activity_signup_login);
        }

    @Override
    public void openLogin(View view) {
        Log.d(clazzz, "Open login Activity");

        Intent intent = new Intent(this, RPSLoginActivity.class);
        startActivity(intent);
        finish();
    }

}
