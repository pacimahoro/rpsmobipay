package com.mobipay.rps.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.mobipay.rps.R;
import com.pesachoice.billpay.activities.PCLoginActivity;
import com.pesachoice.billpay.activities.helpers.DismissKeyboardListener;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCUser;

import org.springframework.util.StringUtils;


/**
 * Created by emmy on 11/23/17.
 */

public class RPSLoginActivity extends PCLoginActivity  {
    private final static String CLAZZZ = RPSLoginActivity.class.getName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rps_activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initializeView();
        selectedCountry = countryCodeMap.get(this.getUserCountryCode(this));
        if (!StringUtils.isEmpty(selectedCountry)) {
            countryPicker1.setText(selectedCountry.substring(selectedCountry.indexOf("+")));
        } else {
            showCountriesCode(this);
        }
        countryPicker1.setOnClickListener(this);
        PCUser user = new PCUser();
        user = getLogedInUser();
        String userName = user.getEmail();
        userNameText.setText(userName);
        configureLoginTypeSelector();
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            isFailedAuth = intent.getExtras().getString("PCCHECOUT");
        }
        // Listen for keyboard dismissal
        findViewById(R.id.login_container).setOnClickListener(new DismissKeyboardListener(this));

    }

    @Override
    protected void initializeView() {
        countryPicker1 = (TextView) findViewById(R.id.countryPicker1);
        loginWithPhoneNumberLayout = (LinearLayout) findViewById(R.id.loging_with_phone_number_layout);
        loginTypes = (Spinner) findViewById(R.id.login_types);
        passwordText = (EditText) findViewById(R.id.password);
        phoneEditText = (EditText) findViewById(R.id.phone);
        userNameText = (EditText) findViewById(R.id.email);
    }

    @Override
    protected Intent createMainTabActivityIntent(PCSpecialUser user) {
        Intent intent = new Intent(this, RPSMainTabActivity.class);
        intent.putExtra(PCPesachoiceConstant.USER_INTENT_EXTRA, user);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }
}


