package com.mobipay.rps.business.receiver;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCUser;

/**
 * Created by emmy on 11/23/17.
 */

public class RPSSignupBroadcastReceiver  extends BroadcastReceiver {

    private final static String CLAZZ = RPSSignupBroadcastReceiver.class.getName();

    private static volatile boolean singletonIsCreated = false;

    private Activity activity;

    private static RPSSignupBroadcastReceiver signupBroadcastReceiver = null;

    public static RPSSignupBroadcastReceiver createOrGetBroadcastReceiver(Activity activity) {
        if (singletonIsCreated) {
            return signupBroadcastReceiver;
        }
        else {
            singletonIsCreated = true;
            return new RPSSignupBroadcastReceiver(activity);
        }
    }



    private RPSSignupBroadcastReceiver(Activity activity){
        this.activity = activity;
    }



    @Override
    public void onReceive(Context context, Intent intent) {
        if (activity != null) {
            Bundle userDataBundle = intent.getBundleExtra(PCPesabusClient.EXTRA_USER_DATA_BUNDLE);
            Object content = userDataBundle.get(PCPesabusClient.EXTRA_USER_DATA);

            Log.v(CLAZZ, "booyeah");
            if (content != null && content instanceof PCUser) {
                Log.v(CLAZZ, "is it here");
                PCUser user = (PCUser)content;
                Log.v(CLAZZ, "first name" + user.getFirstName());
            }
        }
    }
}
