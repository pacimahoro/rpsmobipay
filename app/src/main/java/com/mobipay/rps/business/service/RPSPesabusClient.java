/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.mobipay.rps.business.service;

/**
 * @author Odilon Senyana
 */
public interface RPSPesabusClient {
	//TODO: URL needs to reside in a resource file insteand. TBD
	public final String URL = "http://devpesabus.herokuapp.com/services";
//	public final String URL = "https://pesabus.herokuapp.com/services";
	public final String CONTACT_URL = "http://www.pesachoice.com/#contact";
	public final String PRIVACY_URL = "https://tuma.herokuapp.com/#privacy";//http://www.pesachoice.com/#privacy
	public final String TERMS_OF_SERVICE_URL = "https://tuma.herokuapp.com/#tos";//http://www.pesachoice.com/#tos
	public final String PAYMENT_SCREEN = "payment";
	/*
	 * Relative paths:
	 */
	// General:
	public final String SIGNUP_RELATIVE_PATH = "/signup";
	public final String LOGIN_RELATIVE_PATH = "/login";
	public final String LOGOUT_RELATIVE_PATH = "/logout";
	public final String COUNTRY_PROFILE_RELATIVE_PATH = "/profile/read";
	public final String MERCHANTS_PROFILE_RELATIVE_PATH = "/profile/merchants";
	public final String CALLING_PROFILE_RELATIVE_PATH = "/user/calling/profile";
	public final String FORGOT_PASSWORD_RELATIVE_PATH = "/login/forgot";
	public final String RESET_PASSWORD_RELATIVE_PATH = "/login/reset";
	public final String GET_EVENTS_RELATIVE_PATH = "/profile/events";
	public final String GET_CAPABILITY_TOKEN = "/token/capability";
	public final String VERIFY_EVENTS_RELATIVE_PATH = "/verify/ticketing";
	public final String VERIFY_EVENTS_USING_NBR_RELATIVE_PATH = "/user/ticket";
	public final String CONFIRM_EVENT_TICKET_USAGE_RELATIVE_PATH = "/user/event";
	public final String PING_SERVER_RELATIVE_PATH = URL;
	public final String SEND_MESSAGE = "/sms";

	// Verification:
	public final String VERIFY_PHONE_RELATIVE_PATH = "/verify/phone";
	public final String COMPLETE_REGISTRATION_RELATIVE_PATH = "/signup/complete";

	//Referral and promotion:
	public final String VERIFY_PROMOTION_CODE_PATH = "/discount/verify";
	public final String SUBMIT_REFERRAL_PATH = "/referral/setup";

	// Related to the user:
	public final String USER_INFO_RELATIVE_PATH = "/user/read";
	public final String IS_USER_AUTHENTICATED_RELATIVE_PATH = "/isauth";
	public final String USER_ACTIVITIES_RELATIVE_PATH = "/user/activities";
	public final String GET_RECEVERS_RELATIVE_PATH = "/user/receivers";

	// Related to the payment card:
	public final String CREATE_PAYMENT_PROFILE_RELATIVE_PATH = "/card/create";
	public final String REPLACE_EXISTING_PAYMENT_RELATIVE_PATH = "/card/replace";

	// Bill payment services:
	public final String PAY_AIRTIME_RELATIVE_PATH = "/pay/airtime";
	public final String PAY_CREDIT_KIVUTEL_RELATIVE_PATH = "/pay/calling/refill";
	public final String PAY_ELECTRICITY_RELATIVE_PATH = "/pay/electricity";
	public final String PAY_INTERNET_RELATIVE_PATH = "/pay/internet";
	public final String PAY_TV_RELATIVE_PATH = "/pay/tv";
	public final String PAY_TUITION_RELATIVE_PATH = "/pay/tuition";
	public final String PAY_WATER_RELATIVE_PATH = "/pay/water";
	public final String PAY_MONEY_TRANSFER_RELATIVE_PATH = "/pay/transfer";
	public final String PAY_PRODUCT_TRANSACTION_RELATIVE_PATH = "/pay/product";
	public final String PAY_TICKET_TRANSACTION_RELATIVE_PATH = "/pay/ticketing";

	// photo id metadata
	public final String SAVE_PHOTO_METADATA_PATH = "/user/photo";

	/*
	 * Extras for intents:
     */
	public final String EXTRA_SERVICE_TYPE = "SERVICE_TYPE";
	public final String EXTRA_SERVICE_DATA = "SERVICE_DATA";
	public final String EXTRA_ACTION_TYPE = "ACTION_TYPE";
	public final String EXTRA_USER_DATA = "USER_DATA";
	public final String EXTRA_USER_DATA_BUNDLE = "USER_DATA_BUNDLE";
	public final String ACTIVITY_ITEM_CLICK = "Transaction Details";

	/**
	 * User Account Options
	 */
	public final String OPTION = "option";
	public final String OPTION_PAYMENT = "Payment info";
	public final String OPTION_HELP = "Help";
	public final String OPTION_SHOW_MY_TICKETS = "My Event Tickets";
	public final String OPTION_REPORT_PROBLEM = "Report a Problem";
	public final String OPTION_PRIVACY = "Privacy";
	public final String OPTION_TERMS_OF_SERVICES = "Terms of Services";
	public final String OPTION_CONTACT_US = "Contact Us";
	public final String OPTION_LOGOUT = "Log out";
	public final String OPTION_MOBILE_NUMBER = "Mobile Number";
	public final String OPTION_CALLING_CREDIT = "Calling Credit";
	public final String OPTION_REFER_FRIENDS = "Invite Friends";
	public final String OPTION_TAKE_PHOTO_ID = "Account Verification";
	public final String EXTRA_URL = "extra_url";
	public final String CARD_DIALOG = "Card Payment Dialog";

	/**
	 * option load contact from Phone
	 */
	public final String OPTION_CONTACTS_FROM_PHONE = "Contacts from phone";

	/**
	 * Enumeration used to list all supported services that the client needs to use.
	 */
	public enum PCServiceType {
		PING,
		SIGN_UP,
		VERIFY_PHONE,
		COMPLETE_REGISTRATION,
		FORGOT_PASSWORD,
		RESET_PASSWORD,
		IS_AUTHENTICATED,
		COUNTRY_PROFILE,
		MERCHANTS_PROFILE,
		LOGIN,
		LOGOUT,
		USER_INFO,
		PAYMENT_PROFILE,
		REPLACE_CARD,
		SEND_AIRTIME,
		PAY_ELECTRICITY,
		PAY_INTERNET,
		PAY_TV,
		PAY_TUITION,
		PAY_WATER,
		TICKETS,
		USER_ACTIVITIES,
		CALLING_REFIL,
		GET_CALLING_PROFILE,
		VERIFY_PROMOTION_CODE,
		REFERRAL_USERS,
		MONEY_TRANSFER_ALIAS,
		MONEY_TRANSFER,
		SELL,
		GET_ALL_RECEIVERS,
		EVENTS,
		EVENTS_BY_NBR,
		SEND_MESSAGE,
		GET_CAPABILITY_TOKEN,
		SAVE_PHOTO_METADATA;
	}

	/**
	 * Checks if the device is connected to the internet.
	 *
	 * @return <b>boolean</b>
	 * <b>{@code true}<b/> if the device is connected to the internet and <b>{@code false}<b/> otherwise
	 */
	boolean isDeviceConnected();
}
