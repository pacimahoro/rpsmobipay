package com.mobipay.rps.business.service;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCPesabusClientImpl;
import com.pesachoice.billpay.business.service.PCSignupController;
import com.pesachoice.billpay.model.PCActivity;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCRequest;
import com.pesachoice.billpay.model.PCUser;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Created by emmy on 11/23/17.
 */

public class RPSPesabusClientImpl extends Service implements RPSPesabusClient {


    private final static String CLAZZZ = RPSPesabusClientImpl.class.getName();

    private final RestTemplate restTemplate = new RestTemplate();
    private PCPesabusClient.PCServiceType serviceChosen ;
    private PCUser userReqData = new PCUser();
    private PCAsyncListener asyncListener;

    public synchronized final void setAsnycListener(PCAsyncListener asyncListener) {
        this.asyncListener = asyncListener;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //TODO: incomplete
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
		/*
		 * TODO: Should this class extend IntentService instead and use its functionalities?
		 * Needs to be investigated further.
		 */
        // On restart the intent will be null
        int result = START_STICKY;
        Bundle extrasBundle = intent.getExtras();
        Object type = extrasBundle.get(PCPesabusClient.EXTRA_SERVICE_TYPE);
        Object requestData = extrasBundle.get(PCPesabusClient.EXTRA_SERVICE_DATA);
        if (type != null && type instanceof PCPesabusClient.PCServiceType) {
            serviceChosen = (PCPesabusClient.PCServiceType) type;
            if (serviceChosen != null) {
                switch (serviceChosen) {
                    case PING:
                        break;
                    case SIGN_UP:
                        if(requestData != null && requestData instanceof PCUser) {
                            userReqData = (PCUser) requestData;
                        }
                        break;
                    default:
                        break; // no service chosen, so no clue what to do
                }
            }
            //TODO: incomplete
            if (isDeviceConnected()) {
                Log.v(CLAZZZ, "Starting the service to pesabus");
                try {
                    RPSPesabusClientImpl.ServiceRunner serviceRunner = new RPSPesabusClientImpl.ServiceRunner(intent, flags, startId);
                    Thread serviceThread = new Thread(serviceRunner);
                    serviceThread.start();
                    serviceThread.join();
                } catch (Throwable exc) {
                    //TODO: Need to handle this a little better
                    Log.e(CLAZZZ, exc.getMessage(), exc);
                }
            }
            else {
                Log.e(CLAZZZ, "Failed to start the service to pesabus for pingin the server," +
                        " because the device is not connected to the internet.");
                // On stop or interruption the service will be terminated gracefully
                result = START_NOT_STICKY;
            }
        }
        else {
            // TODO: What do we do here? Just ping the server since there is not service type, or just return without doing anything?
        }
        // Free up the service's resources
        stopSelf();

        return result;
    }


    @Override
    public boolean isDeviceConnected() {
        boolean deviceConnected = false;
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            deviceConnected = true;
        }
        return deviceConnected;
    }






    protected class ServiceRunner implements Runnable {
        private Intent serviceIntent;
        private int serviceFlags;
        private int serviceStartId;

        public ServiceRunner(Intent intent, int flags, int startId) {
            synchronized(this) {
                this.serviceIntent = intent;
                this.serviceFlags = flags;
                this.serviceStartId = startId;
            }
        }
        public void run() {
			/*
			 *  TODO: From Dev Guide:
			 *  a service can protect individual IPC calls into it with permissions,
			 *  by calling the checkCallingPermission(String) method before executing the implementation of that call.
			 */
            synchronized(this) {
                try {
                    restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                    Intent intent = null;
                    if (RPSPesabusClientImpl.this.serviceChosen != null) {
                        switch  (RPSPesabusClientImpl.this.serviceChosen) {
                            case PING:
                                PCData genericData = pingServer();
								/*
								 *FIXME: This section needs to follow same behavior that mimics what is SIGN_UP case below
								 */
                                intent = new Intent("com.pesachoice.tests.com.pesachoice.billpay.business.SERVER_INFO");
                                intent.putExtra(PCPesabusClient.EXTRA_ACTION_TYPE, genericData.getActionType());
                                break;
                            case SIGN_UP:
                                final PCUser userData = new PCUser();
                                userData.setUserName("some_user");
                                //TODO: provide a dynamic way of creating a new user every time we are about to register a new customer
                                userData.setEmail("some_user532@pesachoice.com");
                                userData.setPwd("blablabla");
                                userData.setFirstName("some");
                                userData.setLastName("user");
                                userData.setPhoneNumber("14055556443");
                                try {
                                    //TODO: May need to use handler class
                                    PCSignupController signupController = (PCSignupController)
                                            PCControllerFactory.constructController(
                                                    PCControllerFactory.PCControllerType.SIGN_UP_CONTROLLER, asyncListener);
                                    if (signupController != null) {
                                        signupController.setRequest(userData);
                                        signupController.setActivity((Activity) asyncListener);
                                        signupController.setServiceType(PCPesabusClient.PCServiceType.SIGN_UP);
                                        signupController.execute(userData);
                                    }

                                } catch (Throwable exc) {
                                    Log.e(CLAZZZ, "Could not handle the process [" + exc.getMessage() + "]");
                                }
                                break;
                            default:
                                break; // No clue what was requested!
                        }
                    }
                    sendBroadcast(intent);
                } catch (Throwable exc) {
                    //TODO: handle the exception appropriately
                    Log.e(CLAZZZ, exc.getMessage(), exc);
                }
            }
        }

        /**
         * Pings the server
         * @return <b>results</b>
         * 				data results after pinging the server
         */
        final PCData pingServer() {
            PCData results = null;
            ResponseEntity<PCData> response = restTemplate.postForEntity(URL, new PCRequest(), PCData.class);
            HttpStatus statusCode = response.getStatusCode();
            if (HttpStatus.OK == statusCode) {
                results = response.getBody();
                Log.v(CLAZZZ, "Done pinging the server");
                Log.v(CLAZZZ, "Action type: [" + results.getActionType() + "]");
                Log.v(CLAZZZ, "Class type: [" + results.getClassType() + "]");
                Log.v(CLAZZZ, "Succeeded? [" + results.isSuccess() + "]");
            }
            else {
                //TODO: not all status codes are to be treated as exception though. This needs to be revisited for sure
                throw new RuntimeException("Unable to complete service call. Status code: " + statusCode);
            }
            return results;
        }

        /**
         * Uses the service for signing up new users.
         * @return <b>results</b>
         * 				data results after signing up a new user
         */
        final PCUser signup() {
            PCUser userRespData = null;
            if (RPSPesabusClientImpl.this.userReqData != null) {
                ResponseEntity<PCUser> response = restTemplate.postForEntity(URL + SIGNUP_RELATIVE_PATH, RPSPesabusClientImpl.this.userReqData, PCUser.class);
                HttpStatus statusCode = response.getStatusCode();
                if (HttpStatus.OK == statusCode) {
                    userRespData = response.getBody();
                    Log.v(CLAZZZ, "Results after signing up [" + userRespData + "]");
                }
                else {
                    //TODO: not all status codes are to be treated as exception though. This needs to be revisited for sure
                    throw new RuntimeException("Unable to complete service call. Status code: " + statusCode);
                }
            }
            return userRespData;
        }
    }
}
