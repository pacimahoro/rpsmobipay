package com.mobipay.rps.fragments;

import com.pesachoice.billpay.fragments.PCProductFAQFragment;
import com.pesachoice.billpay.model.PCFAQData;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Created by emmy on 10/01/2018.
 */

public class RPSProductFAQFragment extends PCProductFAQFragment {


    @Override
    protected List<PCFAQData> getFaqsAndAnswers(){
        BufferedReader reader = null;
        StringBuffer buffer = new StringBuffer();
        try {
            reader = new BufferedReader(
                    new InputStreamReader(getActivity().getAssets().open("rps_product_faq.json")));

            // do reading, loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                buffer.append(mLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return jsonToFAQList(buffer.toString());
    }
}
