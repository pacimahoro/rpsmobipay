package com.mobipay.rps.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import com.pesachoice.billpay.fragments.PCWebViewFragment;
import com.mobipay.rps.activities.RPSAccountDetailsActivity;

/**
 * Created by emmy on 12/02/2018.
 */

public class RPSWebViewFragment extends PCWebViewFragment {


    private ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this pcOnPhoneContactLoad
        View rootView = inflater.inflate(com.pesachoice.billpay.activities.R.layout.pc_webview_fragment, container, false);
        WebView webView = (WebView) rootView.findViewById(com.pesachoice.billpay.activities.R.id.display_webcontent);
        progressBar = (ProgressBar) rootView.findViewById(com.pesachoice.billpay.activities.R.id.progressBar);
        webView.setWebChromeClient(new WebChromeClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted (WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progressBar.setVisibility(ProgressBar.VISIBLE);
            }

            @Override
            public void onPageFinished (WebView view, String url) {
                super.onPageFinished(view, url);
                progressBar.setVisibility(ProgressBar.GONE);
            }
        });

        webView.loadUrl(((RPSAccountDetailsActivity) getActivity()).getURLrps());
        webView.requestFocus();

        return rootView;
    }

}
