package com.mobipay.rps.fragments;


import com.mobipay.rps.R;
import com.pesachoice.billpay.fragments.PCCountryAndProductAdapter;
import com.pesachoice.billpay.fragments.PCSupportedCountriesFragment;
import com.pesachoice.billpay.model.PCCountryProfile;




import java.util.ArrayList;
import java.util.List;

/**
 * Created by emmy on 11/21/17.
 */

public class RPSSupportedCountriesFragment extends PCSupportedCountriesFragment {

    public RPSSupportedCountriesFragment() {

    }


    @Override
    public void setupMainView(PCCountryProfile countryProfile) {
        //setup countries list
        List<String> countryNames = new ArrayList<>();

        countryNames.add("Zimbabwe");
        List<Integer> images = new ArrayList<>();
        images.add(R.drawable.icn_zimbabwe);
        if (getActivity() != null) {
            adapter = new PCCountryAndProductAdapter(getActivity(), countryNames, images);
            adapter.setTypeOfDatatToAdapt(TypeOfDataToAdapt.COUNTRIES);
            mainFragmentLayout.setAdapter(adapter);
        }
    }
}
