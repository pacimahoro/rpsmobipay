package com.mobipay.rps.utils;

import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.pesachoice.billpay.activities.helpers.PCPhotoUpload;
import com.pesachoice.billpay.utils.PCProfilePictureUtil;

import org.springframework.util.StringUtils;

/**
 * Created by emmy on 31/01/2018.
 */

public class RPSProfilePictureUtil {
    public static final String storage_bucket = "rpsmobipay.appspot.com";
    private static StorageReference storageRef;
    private static Uri downloadUrl;
    private static PCPhotoUpload activity;
    public static final String main_forlder ="mobipay_ids";
    private PCProfilePictureUtil profile;

    public static void initializeBucket(PCPhotoUpload pcPhotoUploadActivity){

        try {
            activity=pcPhotoUploadActivity;
            FirebaseStorage storage = FirebaseStorage.getInstance();
            if(storage!=null){
                // Create a storage reference from our app
                storageRef=storage.getReferenceFromUrl("gs://"+storage_bucket);
                if(storageRef!=null){
                    //get reference to our storage directory
                    storageRef.child(main_forlder);
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static boolean uploadPhoto(String photoName, byte[] imageToUpload){

        if(storageRef!=null && !StringUtils.isEmpty(photoName)&& imageToUpload!=null){

            // Create a reference to 'mobipay_ids/photoName'
            StorageReference pesaProfilePicRef = storageRef.child("mobipay_ids/"+photoName);
            UploadTask uploadTask = pesaProfilePicRef.putBytes(imageToUpload);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    downloadUrl = taskSnapshot.getDownloadUrl();
                    if(activity!=null){
                        activity.onFinishedPhotoUploading(downloadUrl);
                    }
                }
            });
        }
        return downloadUrl==null?false:true;
    }
}
