package tests.com.mobipay.rps.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.pesachoice.billpay.business.PCPesabusClient;

/**
 * Created by emmy on 11/23/17.
 */

public class SignUpTestBroadcastReceiver extends BroadcastReceiver {

    private static final String CLAZZ = SignUpTestBroadcastReceiver.class.getName();
    @Override
    public void onReceive(Context context, Intent intent) {
        //            Uri data = intent.getData();
        Log.v(CLAZZ, "Got the following");
        String type = intent.getStringExtra(PCPesabusClient.EXTRA_ACTION_TYPE);
        Log.v(CLAZZ, "Got the following : [" + type + "]");
    }
}
