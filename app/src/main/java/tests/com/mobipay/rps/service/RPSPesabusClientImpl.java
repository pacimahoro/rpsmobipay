package tests.com.mobipay.rps.service;

import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCActivity;

/**
 * Created by emmy on 11/23/17.
 */

public class RPSPesabusClientImpl implements RPSPesabusClient{

    @Override
    public boolean isDeviceConnected() {
        return false;
    }

    public static PCPesabusClient.PCServiceType getServiceType(PCActivity.PCPaymentProcessingType processingType) {
        PCPesabusClient.PCServiceType serviceType = null;
        if (processingType == PCActivity.PCPaymentProcessingType.AIR_TIME) {
            serviceType = PCPesabusClient.PCServiceType.SEND_AIRTIME;
        }
        else if (processingType == PCActivity.PCPaymentProcessingType.CALLING) {
            serviceType = PCPesabusClient.PCServiceType.CALLING_REFIL;
        }
        else if (processingType == PCActivity.PCPaymentProcessingType.ELECTRICITY_BILL) {
            serviceType = PCPesabusClient.PCServiceType.PAY_ELECTRICITY;
        }
        else if (processingType == PCActivity.PCPaymentProcessingType.INTERNET_BILL) {
            serviceType = PCPesabusClient.PCServiceType.PAY_INTERNET;
        }
        else if (processingType == PCActivity.PCPaymentProcessingType.WATER) {
            serviceType = PCPesabusClient.PCServiceType.PAY_WATER;
        }
        else if (processingType == PCActivity.PCPaymentProcessingType.MONEY_TRANSFER) {
            serviceType = PCPesabusClient.PCServiceType.MONEY_TRANSFER;
        }
        else if (processingType == PCActivity.PCPaymentProcessingType.TUITION) {
            serviceType = PCPesabusClient.PCServiceType.PAY_TUITION;
        }
        else if (processingType == PCActivity.PCPaymentProcessingType.TV_BILL) {
            serviceType = PCPesabusClient.PCServiceType.PAY_TV;
        }
        else if (processingType == PCActivity.PCPaymentProcessingType.RETAIL_PRODUCTS) {
            serviceType = PCPesabusClient.PCServiceType.SELL;
        }
        else if (processingType == PCActivity.PCPaymentProcessingType.EVENT_TICKETS) {
            serviceType = PCPesabusClient.PCServiceType.TICKETS;
        }
        return serviceType;
    }
}
