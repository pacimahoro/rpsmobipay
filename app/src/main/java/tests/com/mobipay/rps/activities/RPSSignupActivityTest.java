package tests.com.mobipay.rps.activities;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.EditText;

import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.utils.PCGeneralUtils;
import com.mobipay.rps.activities.RPSLoginActivity;
import com.mobipay.rps.activities.RPSSignupActivity;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by emmy on 10/01/2018.
 */

public class RPSSignupActivityTest extends ActivityInstrumentationTestCase2<RPSSignupActivity> {


    private static final String CLAZZ = RPSSignupActivityTest.class.getName();
    private RPSSignupActivity rpsSignupActivity;

    public RPSSignupActivityTest () {

        super(CLAZZ,RPSSignupActivity.class);
    }


    @Before
    public void setUp() throws Exception {
        super.setUp();
        setActivityInitialTouchMode(false);
        rpsSignupActivity = getActivity();

    }


    @Test
    public void testOverallMechanism() {
        assertNotNull(rpsSignupActivity);
        //TODO: incomplete test case
    }



    @Test
    public void testAllRequiredField() {
        /**
         * Test all required field positive case.
         */
        EditText emailText = (EditText) rpsSignupActivity.findViewById(com.pesachoice.billpay.activities.R.id.email);
        EditText phoneText = (EditText) rpsSignupActivity.findViewById(com.pesachoice.billpay.activities.R.id.phone);
        EditText firstNameText = (EditText)rpsSignupActivity.findViewById(com.pesachoice.billpay.activities.R.id.firstName);
        EditText lastNameText = (EditText) rpsSignupActivity.findViewById(com.pesachoice.billpay.activities.R.id.lastName);
        EditText pwdText = (EditText) rpsSignupActivity.findViewById(com.pesachoice.billpay.activities.R.id.password);

        assertNotNull(emailText);
        assertNotNull(phoneText);
        assertNotNull(firstNameText);
        assertNotNull(lastNameText);
        assertNotNull(pwdText);
        /**
         * Test all required field negative caase.
         */
        emailText = null;
        firstNameText = null;

        assertNull(emailText);
        assertNotNull(phoneText);
        assertNull(firstNameText);
        assertNotNull(lastNameText);
        assertNotNull(pwdText);
    }


    @Test
    public void testEmptyFieldValidation() throws PCGenericError {

        try {
            boolean result =  rpsSignupActivity.validCustomerInfo("George","Kagabo","john@smith.com","+1 4056667777","");
        } catch (PCGenericError error) {
            assertEquals(error.getMessage(), rpsSignupActivity.getResources().getString(com.pesachoice.billpay.activities.R.string.sign_error_missing_info));
        }


        /**
         * Test empty field validation positive case.
         */

        String phone = "+1 4056667777";
        String countryCode = "+1";

        // First check for any empty fields or in wrong formart
        if (PCGeneralUtils.isValidPhone(phone.substring(1), countryCode)) {


            assertTrue(rpsSignupActivity.validCustomerInfo("George","Kagabo","john@smith.com","+1 4056667777","emmanuel"));

        }

    }


    @Test
    public void testInvalidPhoneFieldValidation() throws PCGenericError{
        try {
            boolean result =  rpsSignupActivity.validCustomerInfo("George","Kagabo","john@smith.com","+1 4056667777","");
        } catch (PCGenericError error) {
            assertEquals(error.getMessage(), rpsSignupActivity.getResources().getString(com.pesachoice.billpay.activities.R.string.sign_error_invalid_phone));
        }
        /**
         * Test invalid phone field validation positive case.
         */

        boolean result =  rpsSignupActivity.validCustomerInfo("Patrick","Ronald","patrick@ronald.com","+250782901278","emmanuel");
        assertTrue(result);

    }

    @Test
    public void testInvalidEmailFieldValidation() {
        rpsSignupActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                EditText emailText = (EditText) rpsSignupActivity.findViewById(com.pesachoice.billpay.activities.R.id.email);
                EditText phoneText = (EditText) rpsSignupActivity.findViewById(com.pesachoice.billpay.activities.R.id.phone);
                EditText firstNameText = (EditText) rpsSignupActivity.findViewById(com.pesachoice.billpay.activities.R.id.firstName);
                EditText lastNameText = (EditText) rpsSignupActivity.findViewById(com.pesachoice.billpay.activities.R.id.lastName);
                EditText pwdText = (EditText) rpsSignupActivity.findViewById(com.pesachoice.billpay.activities.R.id.password);

                emailText.setText("johnsmith.com");
                phoneText.setText("+1 4056667777");
                firstNameText.setText("George");
                lastNameText.setText("Camaro");
                pwdText.setText("test123");

                String first = firstNameText.getText().toString();
                String last = lastNameText.getText().toString();




                try {
                    rpsSignupActivity.validCustomerInfo(
                            first, last,
                            emailText.getText().toString(),
                            phoneText.getText().toString(),
                            pwdText.getText().toString()
                    );
                } catch (Exception error) {
                    assertEquals(error.getMessage(), rpsSignupActivity.getResources().getString(com.pesachoice.billpay.activities.R.string.sign_error_invalid_email));
                }

                /**
                 * Test invalid email field validation positive case.
                 */

                emailText.setText("john@smith.com");
                try {
                    boolean result = rpsSignupActivity.validCustomerInfo(
                            first, last,
                            emailText.getText().toString(),
                            phoneText.getText().toString(),
                            pwdText.getText().toString()
                    );
                    assertTrue(result);
                } catch (Exception error) {
                    error.getMessage();
                }
            }
        });
    }



}
