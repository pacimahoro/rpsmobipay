package tests.com.mobipay.rps.activities;

import android.test.ActivityInstrumentationTestCase2;
import android.view.MenuItem;

import com.mobipay.rps.activities.RPSAccountDetailsActivity;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by emmy on 10/01/2018.
 */

public class RPSAccountDetailsActivityTest extends ActivityInstrumentationTestCase2<RPSAccountDetailsActivity> {

    private static final String CLAZZ = RPSAccountDetailsActivityTest.class.getName();
    private RPSAccountDetailsActivity accountDetailsActivity;
     public  RPSAccountDetailsActivityTest () {
         super(CLAZZ,RPSAccountDetailsActivity.class);

    }

    @Before
    protected void setUp () throws Exception {
        super.setUp();
        setActivityInitialTouchMode(false);
        accountDetailsActivity = getActivity();
    }

    @Test
    public void testOnOptionsItemSelected () {
        MenuItem item = (MenuItem) getActivity();
        Boolean itemSelectedResult = accountDetailsActivity.onOptionsItemSelected(item);
        assertTrue(itemSelectedResult);

    }


}
